import { defineConfig } from 'umi';
// import extraPostCSSPlugins from './extraPostCSSPlugins.js';
import extraBabelPlugins from './extraBabelPlugins.js';
// import lessLoader from './lessLoader';
// import routes from '../src/routes.js';


export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  // base: '/_api_op/dist/',

  // publicPath: '/_api_op/dist/',
  


  history: { type: 'hash' },
  hash: true,
  title: false,
  antd: {
    dark: false,
    compact: true,
  },
  // routes,
  proxy: {
    '/_api_op': {
      target: 'http://localhost/',
      changeOrigin: true,
      // pathRewrite: { '^/_api_op': '' },
    },
    
  },

  // lessLoader,
  // extraPostCSSPlugins,
  extraBabelPlugins,
  // mpa: {},
});
