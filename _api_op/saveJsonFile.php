<?php
require_once("_inc.php");

$thefilename = isset($_REQUEST['thefilename']) ? $_REQUEST['thefilename'] : "";
$savePath = API_PATH . $thefilename;

$jsondata = file_get_contents('php://input');

// if (isset($jsondata)) {
//     OutMsg(1,  'php://input data is null');
//     die;
// }

if ($thefilename === "") {
    OutMsg(1,  'thefilename is noset');
    die;
}

$result =  file_put_contents($savePath, $jsondata);
OutMsg(0,  $thefilename . ',' . $result);
