<?php
require_once("_inc.php");

$thefilename = isset($_REQUEST['thefilename']) ? $_REQUEST['thefilename'] : "nullthefilename";
$savePath = API_PATH . $thefilename;


if (file_exists($savePath)) {
    echo file_get_contents($savePath);
} else {
    $jsondata = '{}';
    echo file_put_contents($savePath, $jsondata);
}
