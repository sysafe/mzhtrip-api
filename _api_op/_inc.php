<?php
header("Content-type:text/html;charset=utf8");


define("API_PATH", "C:/wwwroot/root_zhtrip.com/api/");
define("DB_PATH", "c://wwwroot/db/_hotelDB.mdb");
// define("LOCATION_FNAME","locations.json");

$adminuname = $_COOKIE["adminuname"];
if ($adminuname === '') {
    header("Location: /_sys_op");
    die();
}

function MyJson_encode($obj)
{
    return json_encode($obj,  JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK, $depth = 8);
}

function connectPDO($dbpath)
{
    try {
        $ConnPDO = new PDO("odbc:driver={microsoft access driver (*.mdb)};dbq=" . $dbpath);
        //echo "Connected success\n";
        return $ConnPDO;
    } catch (Exception $e) {
        echo "Failed:" . iconv('GB2312', 'UTF-8', $e->getMessage());
    }
}
function gbk2utf8($data)
{
    if (is_array($data)) {
        return array_map('gbk2utf8', $data);
    }
    return mb_convert_encoding($data, 'UTF-8', 'GBK');
}

function OutMsg($status, $result)
{
    $obj = new stdClass;
    $obj->status =  $status;
    $obj->data =  $result;
    $obj->juestnow = date('Y-m-d h:i:s', time());

    $obj = json_encode($obj,  JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK, $depth = 8);
    echo $obj;
}
