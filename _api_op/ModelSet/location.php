<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>城市设置</title>
  <script type="text/javascript" src="../utils/jquery.min.js"></script>
  <script src="https://cdn.bootcdn.net/ajax/libs/axios/0.20.0/axios.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue"></script>
</head>

<body>
  <?php include("_menu_model.php"); ?>
  <div style="text-align: center;">
    <div>
      <button id="add">添加</button>
    </div>
    <hr />

    <div id="app">
      <ul>
        <li v-for="item in locals.province" :key="item.name" v-on:click="showcity(item.name)">{{ item.name }}</li>
      </ul>
      <ul>
        <li v-for="item in locals.city" :key="item.name" v-on:click="showarea(item.id)">{{item.name}}</li>
      </ul>
      <ul>
        <li v-for="item in locals.area" :key="item.name">{{item.name}}</li>
      </ul>
      <button v-on:click="showit">showit</button>

    </div>
  </div>

  <script>
    var myLocation = {
      province: [{
        "name": "广东省",
        "id": "440000000000"
      }, ],
      city: [{
        "province": "广东省",
        "name": "珠海市",
        "id": "440400000000"
      }, {
        "province": "广东省",
        "name": "深圳市",
        "id": "440300000000"
      }, ],
      area: {
        "440300000000": [{
            "city": "深圳市",
            "name": "市辖区",
            "id": "440301000000"
          },
          {
            "city": "深圳市",
            "name": "罗湖区",
            "id": "440303000000"
          },
          {
            "city": "深圳市",
            "name": "福田区",
            "id": "440304000000"
          },
          {
            "city": "深圳市",
            "name": "南山区",
            "id": "440305000000"
          },
          {
            "city": "深圳市",
            "name": "宝安区",
            "id": "440306000000"
          },
          {
            "city": "深圳市",
            "name": "龙岗区",
            "id": "440307000000"
          },
          {
            "city": "深圳市",
            "name": "盐田区",
            "id": "440308000000"
          },
          {
            "city": "深圳市",
            "name": "龙华区",
            "id": "440309000000"
          },
          {
            "city": "深圳市",
            "name": "坪山区",
            "id": "440310000000"
          },
          {
            "city": "深圳市",
            "name": "光明区",
            "id": "440311000000"
          }
        ],
        "440400000000": [{
            "city": "珠海市",
            "name": "市辖区",
            "id": "440401000000"
          },
          {
            "city": "珠海市",
            "name": "香洲区",
            "id": "440402000000"
          },
          {
            "city": "珠海市",
            "name": "斗门区",
            "id": "440403000000"
          },
          {
            "city": "珠海市",
            "name": "金湾区",
            "id": "440404000000"
          }
        ],
      },
    }



    var app = new Vue({
      el: '#app',
      data: {
        localSrc: {
          province: {},
          city: {},
          area: {},
        },
        locals: {
          province: {},
          city: {},
          area: {},
        },
      },
      created: function() {


      },
      mounted: function() {
        this.request();



      },
      methods: {
        showit() {
          console.log(JSON.stringify(
            this.locals.area
          ))
        },
        showcity(province) {
          console.log(province)
          var city = this.localSrc.city.map((item) => {
            if (item['province'] === province) {
              return item;
            }
          }) || [];
          this.locals.city = city;

        },
        showarea(id) {
          this.locals.area = this.localSrc.area[id];
        },

        request() {
          axios.get('/api/locations.json?c')
            .then(res => {
              this.localSrc = res.data;
              this.locals.province = this.localSrc.province;
              // this.locals.city = res.data.city;
              console.log(JSON.stringify(this.localSrc))
            })
            .catch(function(error) { // 请求失败处理
              console.log(error);
            });
        }
      }
    })

    // .then(function(myJson) {
    //   console.log(myJson);
    // });


    $(document).ready(function() {




      $("#add").on("click", function() {

        var request = $.ajax({
          type: "post",
          url: "../saveJsonFile.php?savename=locations.json",
          processData: false,
          data: JSON.stringify(myLocation)
        });

        request.done(function(msg) {
          alert(msg);
          console.log(msg);
        });

        request.fail(function(jqXHR, textStatus) {
          alert("Request failed: " + textStatus);
        });

        // console.log(JSON.stringify(province))
      });





    });
  </script>
</body>

</html>