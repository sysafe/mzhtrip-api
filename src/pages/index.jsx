import React from 'react';
import { Link } from 'umi'
import styles from './index.less';

export default () => {
  return (
    <div style={{textAlign:'center'}}>
      <h1 className={styles.title}>后端设置</h1>
      <Link to='/locationModel'>城市设置</Link>
    </div>
  );
}
