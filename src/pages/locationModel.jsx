import React from 'react';
import { connect } from 'umi';
import { Table, Space, Card, Popconfirm } from 'antd';

import LocationSelect from '../components/LocationSelect';

const Locations = ({ locationModel, dispatch, loading }) => {
  let { locations } = locationModel;
  // 省份的children 转化为数组
  locations = ObjToArr(locations);
  // 循环 城市 级
  locations = locations.map(item => {
    let children = item.children || {};
    children = ObjToArr(children);
    return { ...item, children };
  });

  function ObjToArr(obj={}) {
    // input obj ,output array
    return Object.keys(obj).map(item => {
      return obj[item];
    });
  }

  // const cityAndChildren = city.map(item => {
  //   let cityid = item.id;
  //   let children = area[cityid];
  //   return { ...item, children };
  // });
  // const provinceAndChildren = province.map(pro => {
  //   let children = cityAndChildren.map(cit => {
  //     if (cit['province'] === pro.name) {
  //       return cit;
  //     }
  //   });
  //   return { ...pro, children };
  // });
  // console.log(provinceAndChildren);

  const columns = [
    {
      title: '名称',
      dataIndex: 'name',
    },
    {
      title: 'ID',
      dataIndex: 'id',
    },

    {
      title: '操作',
      dataIndex: '',
      render: (text, record, index) => (
        <Space size="middle">
          {record.city ? null : (
            <Popconfirm
              title="Are you sure delete this task?"
              onConfirm={() => {
                dispatch({
                  type: 'locationModel/deleteLocation',
                  payload: { id: record.id },
                });
              }}
              onCancel={() => {}}
              okText="确定"
              cancelText="关闭"
            >
              <a>Delete</a>
            </Popconfirm>
          )}
        </Space>
      ),
    },
  ];

  return (
    <div style={{ width: '800px', padding: '10px' }}>
      <Card title="城市区域管理" extra={<LocationSelect />}>
        <Table
          loading={loading.global}
          columns={columns}
          dataSource={locations}
          rowKey="id"
        />
      </Card>
    </div>
  );
};
export default connect(locationModel => locationModel)(Locations);

// locations = [
//   {
//     name: '山西省',
//     id: '140000000000',
//     isLeaf: false,
//     children: [
//       {
//         province: '山西省',
//         name: '晋城市',
//         id: '140500000000',
//         children: [
//           { city: '晋城市', name: '市辖区', id: '140501000000' },
//           { city: '晋城市', name: '城区', id: '140502000000' },
//           { city: '晋城市', name: '沁水县', id: '140521000000' },
//           { city: '晋城市', name: '阳城县', id: '140522000000' },
//           { city: '晋城市', name: '陵川县', id: '140524000000' },
//           { city: '晋城市', name: '泽州县', id: '140525000000' },
//           { city: '晋城市', name: '高平市', id: '140581000000' },
//         ],
//       },
//     ],
//   },
// ];
