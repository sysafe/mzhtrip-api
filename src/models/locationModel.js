import { request } from 'umi';
import { message } from 'antd';
import config from '@/config'

export default {
  namespace: 'locationModel',

  state: {
    locations: [],
    allprovince: [],
    allcity: [],
    allarea: {},
  },

  reducers: {
    save(state, action) {
      return { ...state, ...action.payload };
    },
  },

  effects: {
    *setup({ payload }, { call, put, select }) {
      let tmp = '/_api_op/china_regions/json/';
      const locations = yield request(
        `/_api_op/readJsonFile.php?thefilename=${config.locationFileName}`,
      );
      const allprovince = yield request(tmp + 'province.json?a=a').then(res =>
        res.map(item => ({ ...item, isLeaf: false })),
      );
      const allcity = yield request(tmp + 'city.json');
      const allarea = yield request(tmp + 'county.json');

      yield put({
        type: 'save',
        payload: { locations, allprovince, allcity, allarea },
      });
    },
    *selectProvince({ payload }, { call, put, select }) {
      //======stp1 第一次点击 省份======
      //isLeaf = false 会触发 loadData
      //不管是不是isLeaf，都会触发 onChange

      const { id } = payload;
      const { locationModel } = yield select();
      let { allprovince, allcity } = locationModel;
      let children = allcity[id];
      allprovince = allprovince.map(item => {
        if (item.id === id) {
          return { ...item, children };
        } else {
          return item;
        }
      });
      yield put({
        type: 'save',
        payload: { allprovince },
      });
    },
    *saveCity({ payload }, { call, put, select }) {
      const { idarr } = payload;
      const [pID, cID] = idarr;
      //=======stp2 第二次点击 城市 ========
      if (cID) {
        //不管是不是isLeaf，都会触发 onChange
        //点击省份列表时（isLeaf: false）cID 为空 -> undefined
        const { locationModel } = yield select();
        let { locations, allprovince, allcity, allarea } = locationModel;
        //取得一个城市的：所有辖区
        let area = allarea[cID];
        //取得一个城市的：所有属性
        let city = allcity[pID].filter(item => item.id === cID)[0];
        // 将辖区放入城市的children
        city = { ...city, children: [...area] };
        // 将城市包装成以城市id为key的对象，方便更新省份的城市列表，更新时方便使用解构赋值，而不用一遍一遍的循环数组 匹配ID
        city = { [cID]: { ...city } };
        // 取得省份所有属性,并去掉省份的城市（城市是在 selectProvince effects里面加入的）
        // 将省份包装成以省份id为key的对象
        if (locations[pID]) {
          var pro = locations[pID];
          var children = { ...pro.children, ...city };
        } else {
          var pro = allprovince.filter(item => item.id === pID)[0];
          var children = city;
        }
        pro = { ...pro, children };
        pro = { [pID]: pro };
        locations = { ...locations, ...pro };

        yield put({
          type: 'save',
          payload: {
            locations,
          },
        });
        yield put({ type: 'saveJson', payload: {} });
      }
    },
    *saveJson({ payload }, { call, put, select }) {
      const { locationModel } = yield select();
      const { locations } = locationModel;
      const res = yield request('/_api_op/saveJsonFile.php', {
        params: { thefilename: config.locationFileName},
        method: 'post',
        useCache: false,
        data: JSON.stringify(locations),
      });
      if (res.status === 0) {
        message.success(res.data);
      } else {
        message.error(res.data);
      }
    },
    *deleteLocation({ payload }, { call, put, select }) {
      const { locationModel } = yield select();
      const { locations } = locationModel;
      const { id } = payload;
      delete locations[id];
      for (const key in locations) {
        delete locations[key].children[id];
      }

      yield put({
        type: 'save',
        payload: { locations },
      });
      yield put({ type: 'saveJson', payload: {} });
    },
  },

  subscriptions: {
    init({ dispatch, history }) {
      return history.listen(({ pathname, query }) => {
        if (pathname === '/locationModel') {
          // 这里dispatch了一个effects要在effects中定义
          dispatch({
            type: 'setup',
          });
        }
      });
    },
  },
};
