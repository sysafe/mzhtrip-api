import React from 'react';
import { connect } from 'umi';
import { Cascader } from 'antd';

const LocationSelect = ({ locationModel, dispatch }) => {
  const { allprovince = [] } = locationModel;
  const loadData = selectedOptions => {
    const targetOption = selectedOptions[selectedOptions.length - 1];
    // targetOption.loading = true;
    const { id } = targetOption;
    dispatch({ type: 'locationModel/selectProvince', payload: { id } });
  };

  return (
    <Cascader
      changeOnSelect
      loadData={loadData}
      fieldNames={{ label: 'name', value: 'id', children: 'children' }}
      options={allprovince}
      onChange={val => {
        dispatch({ type: 'locationModel/saveCity', payload: { idarr: val } });
      }}
      placeholder="添加城市"
    />
  );
};

export default connect(locationModel => locationModel)(LocationSelect);
